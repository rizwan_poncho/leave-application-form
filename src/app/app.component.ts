import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'leave-application-form';

  leaveType: any = [
    {
      type: 'Annual Leave',
    },
    {
      type: 'Sick Leave',
    },
    {
      type: 'Leave Without Pay',
    },
  ];

  supervisor: any = [
    {
      type: 'S. M. Rafi',
    },
    {
      type: 'Mahmudul Hasan',
    },
  ];
  applicationForm: any = FormGroup;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.applicationForm = this.fb.group({
      name: [''],
      designation: [''],
      typeOfLeave: [''],
      startDate: [''],
      endDate: [''],
      time: [''],
      supervisor: [''],
      reason: [''],
    });
  }

  submit() {
    console.log(this.applicationForm.value);
  }
}
